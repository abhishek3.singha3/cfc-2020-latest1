import React from 'react';
import { StyleSheet, View, Image, Text, TouchableOpacity, Button, Linking } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

const styles = StyleSheet.create({
  center: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#FFFFFF'
  },
  centerRight: {
    paddingLeft:200
  },
  scroll: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 25,
    paddingTop: 75
  },
  image: {
    alignSelf: 'flex-start',
    height: '20%',
    width:'50%',
    resizeMode: 'contain'
  },
  title: {
    fontFamily: 'IBMPlexSans-Medium',
    fontSize: 36,
    color: '#140c8d',
    paddingBottom: 15
  },
  subtitle: {
    fontFamily: 'IBMPlexSans-Light',
    fontSize: 24,
    color: '#323232',
    textDecorationColor: '#D0E2FF',
    textDecorationLine: 'underline',
    paddingBottom: 5,
    paddingTop: 20
  },
  firstcontent: {
    fontFamily: 'IBMPlexSans-Light',
    color: '#323232',
    marginTop: 20,
    marginBottom: 10,
    fontSize: 16
  },
  tagline: {
    fontFamily: 'IBMPlexSans-Light',
    color: '#323232',
    marginTop: 10,
    marginBottom: 10,
    fontSize: 14
  },
  content: {
    fontFamily: 'IBMPlexSans-Light',
    color: '#323232',
    marginTop: 10,
    marginBottom: 10,
    fontSize: 16
  },
  buttonGroup: {
    flex: 1,
    paddingTop: 15,
    width: 175
  },
  button: {
    backgroundColor: '#1062FE',
    color: '#FFFFFF',
    fontFamily: 'IBMPlexSans-Medium',
    fontSize: 16,
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    marginTop: 15
  }
});

const Home = ({ navigation }) => (
  <View style={styles.center}>
    <Button style={styles.centerRight}  
        title="Sign In / Sign Up"
        onPress={() => navigation.navigate('Contribute')}
      />
    <ScrollView style={styles.scroll}>
      <Text style={styles.title}>CommunityConnect</Text>
      <Text style={styles.tagline}>Delivering necessities at the time of crisis!</Text>
      <Text style={styles.firstcontent}>
        There is a growing interest in enabling communities to cooperate among
        themselves to solve problems in times of crisis, whether it be to
        advertise where supplies are held, offer assistance for collections, or
        other local services like volunteer deliveries.
      </Text>
      <Text style={styles.content}>
        What is needed is a solution that empowers communities to easily connect
        and provide this information to each other.
      </Text>
      <Text style={styles.content}>
        Our app provides a solution to this concern by connecting the community on 
        a single platform where a person can choose to contribute resources, be it certain
        Commodities or Services. The same platform can be leveraged by volunteers or 
        individuals to access the donated resources or services. It can also be used to raise
        an SOS which will record the location of sender for help.
      </Text>
    </ScrollView>
  </View>
);

export default Home;
