import React from 'react';
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Alert } from 'react-native';
import { add, userID } from '../lib/utils'



const LoginScreen = function ({ navigation }) {
  
  const styles = StyleSheet.create({
    outerView: {
      flex: 1,
      padding: 22,
      backgroundColor: '#FFF'
    },
    splitView: {
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
    typeArea: {
      width: '40%'
    },
    label: {
      fontFamily: 'IBMPlexSans-Medium',
      color: '#000',
      fontSize: 14,
      paddingBottom: 5
    },
    selector: {
      fontFamily: 'IBMPlexSans-Medium',
      borderColor: '#D0E2FF',
      borderWidth: 2,
      padding: 16,
      marginBottom: 25
    },
    quantityArea: {
      width: '40%'
    },
    textInput: {
      fontFamily: 'IBMPlexSans-Medium',
      flex: 1,
      borderColor: '#D0E2FF',
      borderWidth: 2,
      padding: 14,
      elevation: 2,
      marginBottom: 25
    },
    checkboxContainer: {
      flexDirection: 'row',
      alignItems: 'flex-start',
      marginBottom: 10
    },
    checkboxLabel: {
      fontFamily: 'IBMPlexSans-Light',
      fontSize: 13
    },
    textInputDisabled: {
      fontFamily: 'IBMPlexSans-Medium',
      backgroundColor: '#f4f4f4',
      color: '#999',
      flex: 1,
      padding: 16,
      elevation: 2,
      marginBottom: 25
    },
    button: {
      backgroundColor: '#1062FE',
      color: '#FFFFFF',
      fontFamily: 'IBMPlexSans-Medium',
      fontSize: 16,
      overflow: 'hidden',
      padding: 12,
      textAlign:'center',
      marginTop: 15
    }
  });

  const sendItem = () => {
    const payload = {
      serviceType:'SOS',
      type:'Help',
      name:'sos',
      description:'sos',
      quantity:'1',
      location:'2',
      contact:'123456798',
      userID:'test.user2'
    };

    const clearItem = { userID: 'test.user2', 
      serviceType:'SOS', 
      type: 'Help', name: '', 
      description: '', 
      location: '', 
      contact: '', 
      quantity: '1' 
    };

    //const [item, setItem] = React.useState(clearItem);

    add(payload)
      .then(() => {
        Alert.alert('Please be patient!', 'Your SOS has been sent.', [{text: 'OK'}]);
        //setItem({ ...clearItem, location: payload.location });
      })
      .catch(err => {
        console.log(err);
        Alert.alert('ERROR', 'Please try again. If the problem persists contact an administrator.', [{text: 'OK'}]);
      });
  };

    return (
  //   <View >
  //   <Text >Your SOS sent. We will connect you shortly.</Text>
  // </View>
  <TouchableOpacity onPress={sendItem}>
          <Text style={styles.button}>Send SOS</Text>
        </TouchableOpacity>
  );
};

export default LoginScreen;
